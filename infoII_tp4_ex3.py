class File:
    def __init__(self):
        self.items = []

    def enqueue(self, item):
        # Insertion de l'élément à la bonne position dans la file en fonction de sa priorité
        index = 0
        while index < len(self.items) and self.items[index][0] <= item[0]:
            index += 1
        self.items.insert(index, item)

        # on peut utiliser une autre méthode avec append pour ajoute l'élément en fonction de sa priorité et pui trier la liste par ordre de priorité croissant
        #self.items.append(item)
        #self.items.sort(key=lambda x: x[0])

    def dequeue(self, item=None):
        if item is None:
            return self.items.pop() #pour remove le dernier de la liste
        else:
            if item in self.items:
                self.items.remove(item) #pour remove item specifique
                return item

    def is_empty(self):
        return (self.items == [])

    def size(self):
        return len(self.items)
    #pour afficher les résulatats
    def afficher(self):
        return self.items

if __name__ == "__main__":
    file = File()
    file.enqueue((4, "Nathan"))
    file.enqueue((1, "Amandine"))
    file.enqueue((3, "Mathias"))
    file.enqueue((2, "Julia"))
    #file.dequeue() #pour dequeue le dernier (Nathan dans ce cas)
    print(file.size())
    print(file.afficher())